import java.util.HashMap;
import java.util.Scanner;
import java.util.Map;
public class Car extends CarTechnicalPreview  {
    private String name_of_car;
    private String color;
    private String type_coupe;
    private String number_vin;
    private int year_of_production;
    private double car_price;
    CarTechnicalPreview technicalPreview;
    Car_equipment equipmentofcar;
    Shopper shopper;




    public Car(){
        name_of_car= "aaa";
        color = "aaa";
        type_coupe = "aaa";
        number_vin = "aaa" ;
        year_of_production = 1;
        car_price  = -1;
        technicalPreview = new CarTechnicalPreview();
        equipmentofcar = new Car_equipment();
        shopper = new Shopper();

    }


     public Car(String name_of_car, String color, String type_coupe,String number_vin, int year_of_production,double car_price,String fuel_type,double volume_of_engine,double power,int number_of_doors,String ABS,String  ESP , String Climatronic, String  Onboard_computer, String  Nawigation,String Name,String Surname,String Adress ){
        this.name_of_car = name_of_car;
        this.color = color;
        this.type_coupe = type_coupe;
        this.number_vin = number_vin ;
        this.year_of_production = year_of_production;
        this.car_price = car_price;
        this.fuel_type = fuel_type;
        this.volume_of_engine = volume_of_engine;
        this.power = power;
         this.number_of_doors = number_of_doors;
         this.ABS = ABS;
         this.ESP = ESP;
         this.Climatronic = Climatronic;
         this.Onboard_computer = Onboard_computer;
         this.Nawigation = Nawigation;
         this.Name = Name;
         this.Surname = Surname;
         this.Adress = Adress;




     }


    public void setName_of_car(String name_of_car){this.name_of_car = name_of_car;}
    public void setColor(String color){this.color = color;}
    public void setType_coupe(String type_coupe){this.type_coupe = type_coupe;}
    public void setNumber_vin(String number_vin){this.number_vin = number_vin;}
    public void setYear_of_production(int year_of_production){this.year_of_production = year_of_production;}
    public void setCar_price(double car_price){this.car_price = car_price;}


    public String getName_of_car(){return name_of_car;}
    public String getColor(){return color;}
    public String getType_coupe(){return type_coupe;}
    public String getNumber_vin(){return number_vin;}
    public int getYear_of_production(){return year_of_production;}
    public double getCar_price(){return car_price;}


    public void SetINFO(){
        System.out.println("Set Name of car");
        Scanner sc = new Scanner(System.in);
        setName_of_car(sc.nextLine());
        System.out.println("Set color of car");
        setColor(sc.nextLine());
        System.out.println("Set type coupe");
        setType_coupe(sc.nextLine());
        System.out.println("Set Number VIN");
        setNumber_vin(sc.nextLine());
        System.out.println("Set year of production");
        setYear_of_production(sc.nextInt());
        System.out.println("Set price of car");
        setCar_price(sc.nextDouble());


    }
    public void ShowINFO(){
        System.out.println("Car name : "+name_of_car);
        System.out.println("Color : "+color);
        System.out.println("Type coupe : "+type_coupe);
        System.out.println("Number VIN : "+number_vin);
        System.out.println("Year of production : "+year_of_production);
        System.out.println("Price : "+car_price);
    }




}
