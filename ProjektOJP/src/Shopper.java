/**
 * Created by czako_000 on 1/18/2016.
 */
public class Shopper  {
    protected String Name;
    protected String Surname;
    protected String Adress;

    Shopper(){
        Name = "aaa";
        Surname = "aaa";
        Adress = "aaa";
    }
    Shopper(String Name,String Surname,String Adress){
        this.Name = Name;
        this.Surname = Surname;
        this.Adress = Adress;
    }
    public void setName(String Name ){
        this.Name = Name;
    }
    public void setSurname(String Surname){
        this.Surname = Surname;
    }
    public void setAdress(String Adress){
        this.Adress = Adress;
    }

    public String getAdress() {
        return Adress;
    }

    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }
}
