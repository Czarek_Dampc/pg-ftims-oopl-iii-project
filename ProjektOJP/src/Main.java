import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.util.HashMap;
import java.util.Map;
import javax.swing.UIManager;
import java.awt.Dimension;
import java.util.Scanner;

public class Main  {





    public static void main(String[]args) {
        String option;
        String listing = "";
        int opc = 0;
        ArrayList<Car> list = new ArrayList<Car>();





        do {
            option = JOptionPane.showInputDialog("Menu\n\n" +
                    "1.Add car and customer\n\n" +
                    "2.Delete Car\n\n" +
                    "3.Search\n\n" +
                    "4.Display\n\n" +
                    "5.Modify\n\n" +
                    "6.Delete all\n\n" +
                    "7.exit\n\n");
            try {
                opc = Integer.parseInt(option);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "error");
                opc = 0;
            }

            switch (opc) {

                case 1:
                    String Name= JOptionPane.showInputDialog("Name of customer :");
                    String Surname = JOptionPane.showInputDialog("Surname of customer :");
                    String Adress = JOptionPane.showInputDialog("Adress of customer (Zip-code,Street,Number) :");
                    String name_of_car = JOptionPane.showInputDialog("Enter a car :");
                    String color = JOptionPane.showInputDialog("Color of car :");
                    String type_coupe = JOptionPane.showInputDialog("Type coupe :");
                    String number_Vin = JOptionPane.showInputDialog("Number VIN :");
                    int year_of_producion = Integer.parseInt(JOptionPane.showInputDialog("Year of production :"));
                    double car_price = Double.parseDouble(JOptionPane.showInputDialog("Price in $ :"));
                    String fuel_type = JOptionPane.showInputDialog("Fuel type :");
                    double volume_of_engine = Double.parseDouble(JOptionPane.showInputDialog("Volume of engine :"));
                    double power = Double.parseDouble(JOptionPane.showInputDialog("Power :"));
                    int number_of_doors = Integer.parseInt(JOptionPane.showInputDialog("Number of doors :"));
                    String ABS = JOptionPane.showInputDialog("ABS :");
                    String ESP = JOptionPane.showInputDialog("ESP :");
                    String Climatronic = JOptionPane.showInputDialog("Climatronic :");
                    String Onboard_Computer = JOptionPane.showInputDialog("Onboard computer :");
                    String Nawigation = JOptionPane.showInputDialog("Nawigation :");


                    list.add(new Car(name_of_car, color, type_coupe, number_Vin, year_of_producion, car_price, fuel_type, volume_of_engine, power, number_of_doors,ABS,ESP,Climatronic,Onboard_Computer,Nawigation, Name, Surname, Adress));

                    for (int i = 0; i < list.size(); i++) {
                        listing += (i + 1)+"\n"+ "Name of customer :" + list.get(i).getName()
                                +"\n"+ "Surname of customer :"+list.get(i).getSurname()
                                +"\n"+"Adress of customer :" +list.get(i).getAdress()
                                +"\n"+ "Car : " + list.get(i).getName_of_car()
                                + "\n" + "Color of  :" + list.get(i).getColor()
                                + "\n" + "Type coupe :" + list.get(i).getType_coupe()
                                + "\n" + "Number Vin  :" + list.get(i).getNumber_vin()
                                + "\n" + "Year of production :" + list.get(i).getYear_of_production()
                                + "\n" + "Fuel type  :" + list.get(i).getFuel_type()
                                + "\n" + "Volume of engine is  :" + list.get(i).getVolume_of_engine()
                                + "\n" + "Car power (hp)  :" + list.get(i).getPower()
                                + "\n" + "Number of seats  :" + list.get(i).getNumber_of_doors()
                                 + "\n" + "ABS  :" + list.get(i).get_ABS()
                                + "\n" + "ESP  :" + list.get(i).get_ESP()
                                 + "\n" + "Climatronic  :" + list.get(i).getClimatronic()
                                + "\n" + "Onboard Computer  :" + list.get(i).getOnboard_computer()
                                + "\n" + "Nawigation  :" + list.get(i).getNawigation();

                    }
                    JOptionPane.showMessageDialog(null, listing);
                    break;

                case 2:
                    String delete_Car = JOptionPane.showInputDialog("Which you want delete : ");
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getName_of_car().equals(delete_Car)) {
                            list.remove(i);
                        }
                        for (int j = 0; j < list.size(); j++) {
                            listing += (j + 1)+"\n" + "Name of customer :" + list.get(i).getName()
                                    +"\n"+ "Surname of customer :"+list.get(i).getSurname()
                                    +"\n"+"Adress of customer :" +list.get(i).getAdress()
                                    +"\n"+ "Car : " + list.get(i).getName_of_car()
                                    + "\n" + "Color of  :" + list.get(i).getColor()
                                    + "\n" + "Type coupe :" + list.get(i).getType_coupe()
                                    + "\n" + "Number Vin  :" + list.get(i).getNumber_vin()
                                    + "\n" + "Year of production :" + list.get(i).getYear_of_production()
                                    + "\n" + "Car price  :" + list.get(i).getCar_price()
                                    + "\n" + "Fuel type  :" + list.get(i).getFuel_type()
                                    + "\n" + "Volume of engine is  :" + list.get(i).getVolume_of_engine()
                                    + "\n" + "Car power (hp)  :" + list.get(i).getPower()
                                    + "\n" + "Number of seats  :" + list.get(i).getNumber_of_doors()
                                    + "\n" + "ABS  :" + list.get(i).get_ABS()
                                    + "\n" + "ESP  :" + list.get(i).get_ESP()
                                    + "\n" + "Climatronic  :" + list.get(i).getClimatronic()
                                    + "\n" + "Onboard Computer  :" + list.get(i).getOnboard_computer()
                                    + "\n" + "Nawigation  :" + list.get(i).getNawigation();
                        }
                        JOptionPane.showMessageDialog(null, listing);
                        JOptionPane.showMessageDialog(null,"Deleted ","",JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                case 3:
                    String search_result = "NOT FOUND";
                    String searched_name = JOptionPane.showInputDialog("Which customer are you searching  ?");
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getSurname().equals(searched_name)) {
                            search_result = "";
                            search_result += "\n"+"Name of customer :" + list.get(i).getName()
                                    +"\n"+ "Surname of customer :"+list.get(i).getSurname()
                                    +"\n"+"Adress of customer :" +list.get(i).getAdress()
                                    +"\n"+ "Car : " + list.get(i).getName_of_car()
                                    + "\n" + "Color of  :" + list.get(i).getColor()
                                    + "\n" + "Type coupe :" + list.get(i).getType_coupe()
                                    + "\n" + "Number Vin  :" + list.get(i).getNumber_vin()
                                    + "\n" + "Year of production :" + list.get(i).getYear_of_production()
                                    + "\n" + "Car price  :" + list.get(i).getCar_price()
                                    + "\n" + "Fuel type  :" + list.get(i).getFuel_type()
                                    + "\n" + "Volume of engine is  :" + list.get(i).getVolume_of_engine()
                                    + "\n" + "Car power (hp)  :" + list.get(i).getPower()
                                    + "\n" + "Number of seats  :" + list.get(i).getNumber_of_doors()
                                    + "\n" + "ABS  :" + list.get(i).get_ABS()
                                    + "\n" + "ESP  :" + list.get(i).get_ESP()
                                    + "\n" + "Climatronic  :" + list.get(i).getClimatronic()
                                    + "\n" + "Onboard Computer  :" + list.get(i).getOnboard_computer()
                                    + "\n" + "Nawigation  :" + list.get(i).getNawigation();
                        }
                    }

                    JOptionPane.showMessageDialog(null, search_result);
                    break;
                case 4:
                    if (!list.isEmpty()) {
                        for (int i = 0; i < list.size(); i++) {
                            listing += (i + 1) +"\n"+"Name of customer :" + list.get(i).getName()
                                    +"\n"+ "Surname of customer :"+list.get(i).getSurname()
                                    +"\n"+"Adress of customer :" +list.get(i).getAdress()
                                    +"\n"+ "Car : " + list.get(i).getName_of_car()
                                    + "\n" + "Color of  :" + list.get(i).getColor()
                                    + "\n" + "Type coupe :" + list.get(i).getType_coupe()
                                    + "\n" + "Number Vin  :" + list.get(i).getNumber_vin()
                                    + "\n" + "Year of production :" + list.get(i).getYear_of_production()
                                    + "\n" + "Car price  :" + list.get(i).getCar_price()
                                    + "\n" + "Fuel type  :" + list.get(i).getFuel_type()
                                    + "\n" + "Volume of engine is  :" + list.get(i).getVolume_of_engine()
                                    + "\n" + "Car power (hp)  :" + list.get(i).getPower()
                                    + "\n" + "Number of seats  :" + list.get(i).getNumber_of_doors()
                                    + "\n" + "ABS  :" + list.get(i).get_ABS()
                                    + "\n" + "ESP  :" + list.get(i).get_ESP()
                                    + "\n" + "Climatronic  :" + list.get(i).getClimatronic()
                                    + "\n" + "Onboard Computer  :" + list.get(i).getOnboard_computer()
                                    + "\n" + "Nawigation  :" + list.get(i).getNawigation();
                        }
                        JOptionPane.showMessageDialog(null, listing);
                    } else {

                        JOptionPane.showMessageDialog(null, "Empety List", "", JOptionPane.WARNING_MESSAGE);
                    }
                    break;
                case 5:
                    String new_Name= JOptionPane.showInputDialog("Name of customer :" );
                    String new_Surname = JOptionPane.showInputDialog("Surname of customer :");
                    String new_Adress = JOptionPane.showInputDialog("Adress of customer (Zip-code,Street,Number) :");
                    String modify_car = JOptionPane.showInputDialog("Which car you want to modify ?");
                    String new_name_of_car = JOptionPane.showInputDialog("Enter a name of car :");
                    String new_color = JOptionPane.showInputDialog("Color of car :");
                    String new_type_coupe = JOptionPane.showInputDialog("Type coupe :");
                    String new_number_Vin = JOptionPane.showInputDialog("Number VIN :");
                    int new_year_of_producion = Integer.parseInt(JOptionPane.showInputDialog("Year of production :"));
                    double new_car_price = Double.parseDouble(JOptionPane.showInputDialog("Price :"));
                    String new_fuel_type = JOptionPane.showInputDialog("Fuel type :");
                    double new_volume_of_engine = Double.parseDouble(JOptionPane.showInputDialog("Volume of engine :"));
                    double new_power = Double.parseDouble(JOptionPane.showInputDialog("Power :"));
                    String new_ABS = JOptionPane.showInputDialog("ABS :");
                    String new_ESP = JOptionPane.showInputDialog("ESP :");
                    String new_Climatronic = JOptionPane.showInputDialog("Climatronic :");
                    String new_Onboard_Computer = JOptionPane.showInputDialog("Onboard computer :");
                    String new_Nawigation = JOptionPane.showInputDialog("Nawigation :");

                    for(int i = 0;i<list.size();i++){
                        if(list.get(i).getName_of_car().equals(modify_car)){
                            list.get(i).getName().equals(new_Name);
                            list.get(i).getSurname().equals(new_Surname);
                            list.get(i).getAdress().equals(new_Adress);
                            list.get(i).setName_of_car(new_name_of_car);
                            list.get(i).setColor(new_color);
                            list.get(i).setType_coupe(new_type_coupe);
                            list.get(i).setNumber_vin(new_number_Vin);
                            list.get(i).setYear_of_production(new_year_of_producion);
                            list.get(i).setCar_price(new_car_price);
                            list.get(i).setFuel_type(new_fuel_type);
                            list.get(i).setVolume_of_engine(new_volume_of_engine);
                            list.get(i).setPower(new_power);
                            list.get(i).set_ABS(new_ABS);
                            list.get(i).set_ESP(new_ESP);
                            list.get(i).set_Climatronic(new_Climatronic);
                            list.get(i).setOnboard_computer(new_Onboard_Computer);
                            list.get(i).setNawigation(new_Nawigation);

                            listing += (i + 1) +"\n"+"Name of customer :" + list.get(i).getName()
                                    +"\n"+ "Surname of customer :"+list.get(i).getSurname()
                                    +"\n"+"Adress of customer :" +list.get(i).getAdress()
                                    +"\n"+ "Car : " + list.get(i).getName_of_car()
                                    + "\n" + "Color of  :" + list.get(i).getColor()
                                    + "\n" + "Type coupe :" + list.get(i).getType_coupe()
                                    + "\n" + "Number Vin  :" + list.get(i).getNumber_vin()
                                    + "\n" + "Year of production :" + list.get(i).getYear_of_production()
                                    + "\n" + "Car price  :" + list.get(i).getCar_price()
                                    + "\n" + "Fuel type  :" + list.get(i).getFuel_type()
                                    + "\n" + "Volume of engine is  :" + list.get(i).getVolume_of_engine()
                                    + "\n" + "Car power (hp)  :" + list.get(i).getPower()
                                    + "\n" + "Number of seats  :" + list.get(i).getNumber_of_doors()
                                    + "\n" + "ABS  :" + list.get(i).get_ABS()
                                    + "\n" + "ESP  :" + list.get(i).get_ESP()
                                    + "\n" + "Climatronic  :" + list.get(i).getClimatronic()
                                    + "\n" + "Onboard Computer  :" + list.get(i).getOnboard_computer()
                                    + "\n" + "Nawigation  :" + list.get(i).getNawigation();
                        JOptionPane.showMessageDialog(null,listing);
                        }
                    }



                    break;
                case 6:
                    list.clear();
                    JOptionPane.showMessageDialog(null,"Empty List","",JOptionPane.WARNING_MESSAGE);
                    break;

                case 7:
                    System.exit(0);
                    break;




            }
        }while(opc!= 9);

















    }
}
