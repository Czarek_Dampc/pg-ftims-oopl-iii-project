/**
 * Created by czako_000 on 1/17/2016.
 */
public class CarTechnicalPreview extends Car_equipment {
    protected String fuel_type;
    protected double volume_of_engine;
    protected double power;
    protected int number_of_doors;



    public CarTechnicalPreview(){
        fuel_type = "aaa";
        volume_of_engine = 1 ;
        power = 1;
       number_of_doors = 1;

    }
    public CarTechnicalPreview(String fuel_type,double volume_of_engine,double power,int number_of_doors){
        this.fuel_type = fuel_type;
        this.volume_of_engine = volume_of_engine;
        this.power = power;
        this.number_of_doors = number_of_doors;
    }

    public void setFuel_type(String fuel_type){this.fuel_type = fuel_type;};
    public void setVolume_of_engine(double volume_of_engine){this.volume_of_engine = volume_of_engine;}
    public void setPower(double power){this.power = power;}
    public void setNumber_of_doors(int number_of_doors){this.number_of_doors = number_of_doors;}


    public String getFuel_type(){return fuel_type;}
    public double getVolume_of_engine(){return volume_of_engine;}
    public double getPower(){ return power;}
    public int getNumber_of_doors(){return number_of_doors;}












}
